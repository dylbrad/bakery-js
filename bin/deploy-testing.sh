#!/bin/bash

read -p "Are you sure you want to deploy to TESTING? (y/n) " -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    now=$(date +'%Y-%m-%d-%H-%M')
    git_branch=$(git rev-parse --abbrev-ref HEAD)
    filename=$now-$git_branch
    ssh_host=bakery-testing

    scp -r build $ssh_host:bakery-js/$filename
    ssh $ssh_host "rm bakery-js/latest; ln -s /home/$ssh_host/bakery-js/$filename /home/$ssh_host/bakery-js/latest"
fi
