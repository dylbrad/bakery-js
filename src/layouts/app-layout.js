import React, { createRef, useEffect } from "react";
import { connect } from "react-redux";
import { Icon, Ref, Sticky, Container, Menu } from "semantic-ui-react";
import { Link, useLocation } from "react-router-dom";

import AppHeader from "components/app-header";
import TrolleyButton from "components/trolley/trolley-button";
import InstagramLink from "components/instagram-link";
import { getBackgroundColor } from "utils/util";
import { getSettings, clearSettings } from "redux/actions/setting-manager";

const AppLayout = ({
  children,
  getSettings,
  clearSettings,
  settingManager,
}) => {
  const { pathname } = useLocation();
  const contextRef = createRef();

  useEffect(() => {
    getSettings();
    return () => clearSettings();
  }, [getSettings, clearSettings]);

  useEffect(() => {
    const favicon = document.getElementById("favicon");
    const frontendFavicon = settingManager.getByName("frontend_favicon").value;
    const bakeryName = settingManager.getByName("bakery_name").value;
    if (frontendFavicon) {
      favicon.href = frontendFavicon;
    }
    if (bakeryName) {
      document.title = bakeryName;
    }
  }, [settingManager]);

  return (
    <Ref innerRef={contextRef}>
      <Container style={{ backgroundColor: getBackgroundColor() }}>
        <AppHeader />
        <InstagramLink />
        <Sticky context={contextRef}>
          <Menu stackable>
            <Menu.Item
              color="orange"
              active={pathname === "/" || pathname.startsWith("/products")}
              as={Link}
              link
              to="/"
            >
              <Icon name="food" />
              Products
            </Menu.Item>
            <Menu.Item
              color="blue"
              active={pathname === "/about"}
              as={Link}
              to="/about"
            >
              <Icon name="info" />
              About
            </Menu.Item>
            <Menu.Item
              color="teal"
              active={pathname === "/contact"}
              as={Link}
              to="/contact"
            >
              <Icon name="mail" />
              Contact
            </Menu.Item>
            <Menu.Item
              position="right"
              active={pathname === "/view-trolley"}
              as={Link}
              to="/view-trolley"
              color="green"
            >
              <TrolleyButton />
            </Menu.Item>
          </Menu>
        </Sticky>
        {children}
        <div style={{ height: "20px" }} />
      </Container>
    </Ref>
  );
};

const mapStateToProps = ({ settingManager }) => ({ settingManager });

const mapDispatchToProps = { getSettings, clearSettings };

export default connect(mapStateToProps, mapDispatchToProps)(AppLayout);
