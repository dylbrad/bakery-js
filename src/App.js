import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { Provider } from "react-redux";

import AppLayout from "layouts/app-layout";
import ViewTrolleyPage from "pages/view-trolley";
import HomePage from "pages/home";
import CustomerForm from "components/customer-form";
import Payment from "components/payment";
import OrderPlaced from "components/order-placed";
import ScrollToTop from "components/scroll-to-top";
import ViewRecipePage from "pages/view-recipe";
import ContactPage from "pages/contact";
import ConfirmOrderPage from "pages/confirm-order";
import OrderConfirmedPage from "pages/order-confirmed";
import GenericNotFound from "pages/404";
import AboutPage from "pages/about";
import EditBookingPage from "pages/edit-booking";

import store from "redux/store";

import "react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css";
import "semantic-ui-css/semantic.min.css";

import "App.css";

const App = () => (
  <Provider store={store}>
    <Router>
      <ScrollToTop />
      <AppLayout>
        <Switch>
          <Route exact path="/orders/:orderId/:confirmationKey">
            <ConfirmOrderPage />
          </Route>
          <Route exact path="/order-confirmed">
            <OrderConfirmedPage />
          </Route>
          <Route exact path="/">
            <HomePage />
          </Route>
          <Route path="/view-trolley">
            <ViewTrolleyPage />
          </Route>
          <Route path="/details">
            <CustomerForm />
          </Route>
          <Route path="/booking">
            <EditBookingPage />
          </Route>
          <Route path="/payment">
            <Payment />
          </Route>
          <Route path="/order-placed">
            <OrderPlaced />
          </Route>
          <Route path="/products/:productId/recipe">
            <ViewRecipePage />
          </Route>
          <Route path="/contact">
            <ContactPage />
          </Route>
          <Route path="/about">
            <AboutPage />
          </Route>
          <Route path="/404" component={GenericNotFound} />
          <Redirect to="/404" />
        </Switch>
      </AppLayout>
    </Router>
  </Provider>
);

export default App;
