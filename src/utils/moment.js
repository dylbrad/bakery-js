export function beginningOfWeek(date) {
  // Sunday is represented as 0, we need to change it to 7 for the calculations
  const dayOfWeek = date.day() === 0 ? 7 : date.day();
  const monday = date.clone().subtract(dayOfWeek - 1, "day");
  monday.set("hour", 0);
  monday.set("minute", 0);
  monday.set("second", 0);
  return monday;
}

export function addDays(date, days) {
  return date.clone().add(days, "day");
}

export function removeDays(date, days) {
  return date.clone().subtract(days, "day");
}
