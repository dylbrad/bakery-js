import React from "react";
import { connect } from "react-redux";
import { Segment } from "semantic-ui-react";

import Markdown from "components/markdown";

const About = ({ settingManager }) => (
  <Segment basic>
    <div style={{ fontSize: "1.5em" }}>
      <Markdown source={settingManager.getByName("about").value} />
    </div>
  </Segment>
);

const mapStateToProps = ({ settingManager }) => ({ settingManager });

export default connect(mapStateToProps)(About);
