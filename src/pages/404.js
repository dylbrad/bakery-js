import React from "react";
import { Segment } from "semantic-ui-react";

const GenericNotFound = () => {
  return (
    <Segment basic>
      <h1>404 - Page not found</h1>
    </Segment>
  );
};

export default GenericNotFound;
