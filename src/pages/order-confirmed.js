import React from "react";
import { connect } from "react-redux";
import { Header, Segment } from "semantic-ui-react";

import { fetchOrder, clearOrder, confirmOrder } from "redux/actions/order";

const ConfirmOrderPage = () => {
  return (
    <Segment basic>
      <Header>Thank you for confirming your email address!</Header>
      <p>
        You will soon receive an email letting you know of the progress of your
        order.
      </p>
    </Segment>
  );
};

const mapStateToProps = ({ delivery }) => ({ delivery });

const mapDispatchToProps = { fetchOrder, clearOrder, confirmOrder };

export default connect(mapStateToProps, mapDispatchToProps)(ConfirmOrderPage);
