import { SET_CUSTOMER, CLEAR_CUSTOMER } from "redux/actions/action-types";

export function setCustomer(customer, callback) {
  return (dispatch, getState) => {
    dispatch({ type: SET_CUSTOMER, payload: customer });
    if (callback) callback(getState().customer);
  };
}

export function clearCustomer() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_CUSTOMER });
  };
}
