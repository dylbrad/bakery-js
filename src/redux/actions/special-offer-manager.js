import axios from "redux/actions/axios";
import {
  GET_SPECIAL_OFFERS,
  CLEAR_SPECIAL_OFFERS,
} from "redux/actions/action-types";

import SpecialOfferManager from "domain-objects/special-offer-manager";
import SpecialOffer from "domain-objects/special-offer";

export function fetchSpecialOffers() {
  return (dispatch, getState) => {
    axios.get("/special_offers/").then((response) =>
      dispatch({
        type: GET_SPECIAL_OFFERS,
        payload: new SpecialOfferManager(
          response.data.data.map(
            (specialOffer) => new SpecialOffer(specialOffer)
          )
        ),
      })
    );
  };
}

export function clearSpecialOffers() {
  return (dispatch, getState) => dispatch({ type: CLEAR_SPECIAL_OFFERS });
}
