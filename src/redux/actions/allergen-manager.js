import axios from "redux/actions/axios";
import { GET_ALLERGENS, CLEAR_ALLERGENS } from "redux/actions/action-types";

import AllergenManager from "domain-objects/allergen-manager";
import Allergen from "domain-objects/allergen";

export function fetchAllergens() {
  return (dispatch, getState) => {
    axios.get("/allergens/").then((response) =>
      dispatch({
        type: GET_ALLERGENS,
        payload: new AllergenManager(
          response.data.data.map((allergen) => new Allergen(allergen))
        ),
      })
    );
  };
}

export function clearAllergens() {
  return (dispatch, getState) => dispatch({ type: CLEAR_ALLERGENS });
}
