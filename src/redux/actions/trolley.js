import {
  ADD_ONE_PRODUCT_TO_TROLLEY,
  REMOVE_ONE_PRODUCT_FROM_TROLLEY,
  DELETE_PRODUCT_FROM_TROLLEY,
  ADD_ONE_SPECIAL_OFFER_TO_TROLLEY,
  REMOVE_ONE_SPECIAL_OFFER_FROM_TROLLEY,
  DELETE_SPECIAL_OFFER_FROM_TROLLEY,
  SET_TROLLEY,
  CLEAR_TROLLEY,
  UPDATE_TROLLEY_DELIVERY_PRICE,
} from "redux/actions/action-types";

import Trolley from "domain-objects/trolley";

export function addOneProduct(product) {
  return (dispatch, getState) => {
    dispatch({ type: ADD_ONE_PRODUCT_TO_TROLLEY, payload: product });
  };
}

export function removeOneProduct(product) {
  return (dispatch, getState) => {
    dispatch({ type: REMOVE_ONE_PRODUCT_FROM_TROLLEY, payload: product });
  };
}

export function deleteProduct(product) {
  return (dispatch, getState) => {
    dispatch({ type: DELETE_PRODUCT_FROM_TROLLEY, payload: product });
  };
}

export function addOneSpecialOffer(specialOffer) {
  return (dispatch, getState) => {
    dispatch({ type: ADD_ONE_SPECIAL_OFFER_TO_TROLLEY, payload: specialOffer });
  };
}

export function removeOneSpecialOffer(specialOffer) {
  return (dispatch, getState) => {
    dispatch({
      type: REMOVE_ONE_SPECIAL_OFFER_FROM_TROLLEY,
      payload: specialOffer,
    });
  };
}

export function deleteSpecialOffer(specialOffer) {
  return (dispatch, getState) => {
    dispatch({
      type: DELETE_SPECIAL_OFFER_FROM_TROLLEY,
      payload: specialOffer,
    });
  };
}

export function setTrolley(trolley) {
  return (dispatch, getState) => {
    dispatch({ type: SET_TROLLEY, payload: new Trolley(trolley) });
  };
}

export function clearTrolley() {
  return async (dispatch, getState) => {
    const deliveryPrice = getState().trolley.delivery_price;
    await dispatch({
      type: CLEAR_TROLLEY,
      payload: new Trolley({ delivery_price: deliveryPrice }),
    });
  };
}

export function updateDeliveryPrice(deliveryPrice) {
  return (dispatch, getState) => {
    dispatch({
      type: UPDATE_TROLLEY_DELIVERY_PRICE,
      payload: deliveryPrice,
    });
  };
}
