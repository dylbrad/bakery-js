import axios from "redux/actions/axios";

import {
  PLACE_ORDER,
  FETCH_ORDER,
  CLEAR_ORDER,
  SET_TROLLEY,
  SET_DELIVERY,
  CONFIRM_ORDER,
  SET_IS_DELIVERY,
  UPDATE_TROLLEY_DELIVERY_PRICE,
} from "redux/actions/action-types";
import { createOrder } from "domain-objects/order";
import { createTrolley } from "domain-objects/trolley";
import Delivery from "domain-objects/delivery";

export function placeOrder() {
  return async (dispatch, getState) => {
    const storeOrder = getState().order;
    const order = createOrder({
      ...storeOrder,
      delivery: getState().delivery,
      trolley: getState().trolley,
      payment: getState().payment,
      customer: getState().customer,
    });
    await axios.post("/orders/", order.renderJSON()).then((response) => {
      dispatch({
        type: PLACE_ORDER,
        payload: createOrder(response.data.data),
      });
    });
  };
}

export function fetchOrder(orderId, confirmationKey) {
  return (dispatch, getState) => {
    axios.get(`/orders/${orderId}/${confirmationKey}`).then((response) => {
      dispatch({
        type: FETCH_ORDER,
        payload: createOrder(response.data.data),
      });
      dispatch({
        type: SET_TROLLEY,
        payload: createTrolley(response.data.data.trolley),
      });
      dispatch({
        type: SET_DELIVERY,
        payload: new Delivery(response.data.data.delivery),
      });
    });
  };
}

export function clearOrder() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_ORDER });
  };
}

export function confirmOrder(orderId, confirmationKey) {
  return (dispatch, getState) => {
    axios.put(`/orders/${orderId}/${confirmationKey}`).then((response) => {
      dispatch({
        type: CONFIRM_ORDER,
        payload: createOrder(response.data.data),
      });
    });
  };
}

export function setIsDelivery(order) {
  return (dispatch, getState) => {
    dispatch({
      type: SET_IS_DELIVERY,
      payload: order,
    });
    const deliveryPrice = getState().settingManager.getByName("delivery_price").value;
    dispatch({
      type: UPDATE_TROLLEY_DELIVERY_PRICE,
      payload: order.is_delivery ? deliveryPrice : 0,
    });
  };
}
