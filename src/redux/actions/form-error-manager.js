import {
  SET_FORM_ERROR_MANAGER,
  CLEAR_FORM_ERROR_MANAGER,
} from "redux/actions/action-types";

import { createFormErrorManager } from "domain-objects/form-error-manager";

export function setFormErrorManager(formErrorManager) {
  return (dispatch, getState) => {
    dispatch({
      type: SET_FORM_ERROR_MANAGER,
      payload: createFormErrorManager(formErrorManager),
    });
  };
}

export function clearFormErrorManager() {
  return (dispatch, getState) => dispatch({ type: CLEAR_FORM_ERROR_MANAGER });
}
