import axios from "redux/actions/axios";
import {
  FETCH_BOOKING_SLOT,
  CLEAR_BOOKING_SLOT,
} from "redux/actions/action-types";
import BookingSlot from "domain-objects/booking-slot";

export function fetchEarliestBookingSlot(callback) {
  return (dispatch, getState) => {
    axios.get("/booking_slots/earliest").then((response) => {
      if (response.data.data)
        dispatch({
          type: FETCH_BOOKING_SLOT,
          payload: new BookingSlot(response.data.data),
        });
    });
  };
}

export function clearBookingSlot() {
  return (dispatch, getState) => {
    dispatch({ type: CLEAR_BOOKING_SLOT });
  };
}
