import { GET_PRODUCTS, CLEAR_PRODUCTS } from "redux/actions/action-types";

import ProductManager from "domain-objects/product-manager";

export function productManagerReducer(state = new ProductManager(), action) {
  switch (action.type) {
    case GET_PRODUCTS: {
      return action.payload;
    }
    case CLEAR_PRODUCTS: {
      return new ProductManager();
    }
    default: {
      return state;
    }
  }
}
