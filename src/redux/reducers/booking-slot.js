import {
  FETCH_BOOKING_SLOT,
  CLEAR_BOOKING_SLOT,
} from "redux/actions/action-types";

import BookingSlot from "domain-objects/booking-slot";

export function bookingSlotReducer(state = new BookingSlot(), action) {
  switch (action.type) {
    case FETCH_BOOKING_SLOT: {
      return action.payload;
    }
    case CLEAR_BOOKING_SLOT: {
      return new BookingSlot();
    }
    default: {
      return state;
    }
  }
}
