import { GET_PRODUCT, CLEAR_PRODUCT } from "redux/actions/action-types";

import Product from "domain-objects/product";

export function productReducer(state = new Product(), action) {
  switch (action.type) {
    case GET_PRODUCT: {
      return action.payload;
    }
    case CLEAR_PRODUCT: {
      return new Product();
    }
    default: {
      return state;
    }
  }
}
