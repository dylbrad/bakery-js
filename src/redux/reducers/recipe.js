import { GET_RECIPE, CLEAR_RECIPE } from "redux/actions/action-types";

import Recipe from "domain-objects/recipe";

export function recipeReducer(state = new Recipe(), action) {
  switch (action.type) {
    case GET_RECIPE: {
      return action.payload;
    }
    case CLEAR_RECIPE: {
      return new Recipe();
    }
    default: {
      return state;
    }
  }
}
