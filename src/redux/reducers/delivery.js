import {
  CLEAR_DELIVERY,
  SET_DELIVERY,
  SET_DELIVERY_MESSAGE,
} from "redux/actions/action-types";

import Delivery from "domain-objects/delivery";

export function deliveryReducer(state = new Delivery(), action) {
  switch (action.type) {
    case SET_DELIVERY_MESSAGE:
    case SET_DELIVERY: {
      return action.payload;
    }
    case CLEAR_DELIVERY: {
      return new Delivery();
    }
    default: {
      return state;
    }
  }
}
