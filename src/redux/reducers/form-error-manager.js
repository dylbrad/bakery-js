import {
  CLEAR_FORM_ERROR_MANAGER,
  SET_FORM_ERROR_MANAGER,
} from "redux/actions/action-types";

import { createFormErrorManager } from "domain-objects/form-error-manager";

export function formErrorManagerReducer(
  state = createFormErrorManager(),
  action
) {
  switch (action.type) {
    case SET_FORM_ERROR_MANAGER: {
      return action.payload;
    }
    case CLEAR_FORM_ERROR_MANAGER: {
      return createFormErrorManager();
    }
    default: {
      return state;
    }
  }
}
