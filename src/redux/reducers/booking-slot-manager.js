import {
  GET_BOOKING_SLOTS,
  CLEAR_BOOKING_SLOTS,
} from "redux/actions/action-types";

import BookingSlotManager from "domain-objects/booking-slot-manager";

export function bookingSlotManagerReducer(
  state = new BookingSlotManager(),
  action
) {
  switch (action.type) {
    case GET_BOOKING_SLOTS: {
      return action.payload;
    }
    case CLEAR_BOOKING_SLOTS: {
      return new BookingSlotManager();
    }
    default: {
      return state;
    }
  }
}
