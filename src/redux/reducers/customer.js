import { CLEAR_CUSTOMER, SET_CUSTOMER } from "redux/actions/action-types";

import Customer from "domain-objects/customer";

export function customerReducer(state = new Customer(), action) {
  switch (action.type) {
    case SET_CUSTOMER: {
      return action.payload;
    }
    case CLEAR_CUSTOMER: {
      return new Customer();
    }
    default: {
      return state;
    }
  }
}
