import {
  GET_SPECIAL_OFFERS,
  CLEAR_SPECIAL_OFFERS,
} from "redux/actions/action-types";

import SpecialOfferManager from "domain-objects/special-offer-manager";

export function specialOfferManagerReducer(
  state = new SpecialOfferManager(),
  action
) {
  switch (action.type) {
    case GET_SPECIAL_OFFERS: {
      return action.payload;
    }
    case CLEAR_SPECIAL_OFFERS: {
      return new SpecialOfferManager();
    }
    default: {
      return state;
    }
  }
}
