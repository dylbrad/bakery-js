import {
  ADD_ONE_PRODUCT_TO_TROLLEY,
  REMOVE_ONE_PRODUCT_FROM_TROLLEY,
  DELETE_PRODUCT_FROM_TROLLEY,
  ADD_ONE_SPECIAL_OFFER_TO_TROLLEY,
  REMOVE_ONE_SPECIAL_OFFER_FROM_TROLLEY,
  DELETE_SPECIAL_OFFER_FROM_TROLLEY,
  SET_TROLLEY,
  CLEAR_TROLLEY,
  UPDATE_TROLLEY_DELIVERY_PRICE,
} from "redux/actions/action-types";

import { createTrolley } from "domain-objects/trolley";

export function trolleyReducer(state = createTrolley(), action) {
  switch (action.type) {
    case UPDATE_TROLLEY_DELIVERY_PRICE: {
      return state.updateDeliveryPrice(action.payload);
    }
    case ADD_ONE_PRODUCT_TO_TROLLEY: {
      return state.addOneProduct(action.payload);
    }
    case REMOVE_ONE_PRODUCT_FROM_TROLLEY: {
      return state.removeOneProduct(action.payload);
    }
    case DELETE_PRODUCT_FROM_TROLLEY: {
      return state.deleteProduct(action.payload);
    }
    case ADD_ONE_SPECIAL_OFFER_TO_TROLLEY: {
      return state.addOneSpecialOffer(action.payload);
    }
    case REMOVE_ONE_SPECIAL_OFFER_FROM_TROLLEY: {
      return state.removeOneSpecialOffer(action.payload);
    }
    case DELETE_SPECIAL_OFFER_FROM_TROLLEY: {
      return state.deleteSpecialOffer(action.payload);
    }
    case CLEAR_TROLLEY:
    case SET_TROLLEY: {
      return action.payload;
    }
    default: {
      return state;
    }
  }
}
