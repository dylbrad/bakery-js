import { GET_ALLERGENS, CLEAR_ALLERGENS } from "redux/actions/action-types";

import AllergenManager from "domain-objects/allergen-manager";

export function allergenManagerReducer(state = new AllergenManager(), action) {
  switch (action.type) {
    case GET_ALLERGENS: {
      return action.payload;
    }
    case CLEAR_ALLERGENS: {
      return new AllergenManager();
    }
    default: {
      return state;
    }
  }
}
