import { combineReducers, applyMiddleware, createStore, compose } from "redux";
import { createLogger } from "redux-logger";
import thunk from "redux-thunk";

import { trolleyReducer } from "redux/reducers/trolley";
import { deliveryReducer } from "redux/reducers/delivery";
import { customerReducer } from "redux/reducers/customer";
import { productManagerReducer } from "redux/reducers/product-manager";
import { productReducer } from "redux/reducers/product";
import { specialOfferManagerReducer } from "redux/reducers/special-offer-manager";
import { recipeReducer } from "redux/reducers/recipe";
import { orderReducer } from "redux/reducers/order";
import { paymentReducer } from "redux/reducers/payment";
import { formErrorManagerReducer } from "redux/reducers/form-error-manager";
import { allergenManagerReducer } from "redux/reducers/allergen-manager";
import { bookingSlotManagerReducer } from "redux/reducers/booking-slot-manager";
import { bookingSlotReducer } from "redux/reducers/booking-slot";
import { settingManagerReducer } from "redux/reducers/setting-manager";

const logger = createLogger({ collapsed: true });

const createStoreWithMiddleware = applyMiddleware(thunk, logger)(createStore);

const enhancers = compose(
  window.devToolsExtension ? window.devToolsExtension() : (f) => f
);

const rootReducer = combineReducers({
  trolley: trolleyReducer,
  delivery: deliveryReducer,
  customer: customerReducer,
  productManager: productManagerReducer,
  product: productReducer,
  specialOfferManager: specialOfferManagerReducer,
  recipe: recipeReducer,
  order: orderReducer,
  payment: paymentReducer,
  formErrorManager: formErrorManagerReducer,
  allergenManager: allergenManagerReducer,
  bookingSlotManager: bookingSlotManagerReducer,
  bookingSlot: bookingSlotReducer,
  settingManager: settingManagerReducer,
});

export default createStoreWithMiddleware(rootReducer, enhancers);
