export default class TrolleyItem {
  constructor({
    id,
    product_id,
    special_offer_id,
    quantity = 0,
    unit_price = 0,
  }) {
    this.id = id;
    this.product_id = product_id;
    this.special_offer_id = special_offer_id;
    this.quantity = quantity;
    this.unit_price = unit_price;
  }

  calculateTotal(format) {
    const output = Math.round(this.quantity * this.unit_price * 100) / 100;
    if (format === "string" && !Number.isInteger(output)) {
      return output.toFixed(2);
    }
    return output;
  }

  isProduct() {
    return !!this.product_id;
  }

  render(field) {
    switch (field) {
      case "unit_price": {
        return Number.isInteger(this[field])
          ? this[field]
          : this[field].toFixed(2);
      }
      default: {
        return this[field];
      }
    }
  }
}
