export default class Customer {
  constructor({
    first_name = "",
    last_name = "",
    email = "",
    phone = "",
    address1 = "",
    address2 = "",
    city = "Dublin",
    county = "Dublin",
  } = {}) {
    this.first_name = first_name;
    this.last_name = last_name;
    this.email = email;
    this.phone = phone;
    this.address1 = address1;
    this.address2 = address2;
    this.city = city;
    this.county = county;
  }

  update(field, value) {
    return new Customer({ ...this, [field]: value });
  }

  isFieldValid(field) {
    switch (field) {
      default: {
        return this[field] !== "";
      }
    }
  }

  isEmpty() {
    return (
      this.first_name === "" &&
      this.last_name === "" &&
      this.email === "" &&
      this.phone === "" &&
      this.address1 === "" &&
      this.address2 === ""
    );
  }

  isValid(isDelivery) {
    if (isDelivery === true) {
      return (
        this.first_name !== "" &&
        this.last_name !== "" &&
        this.email !== "" &&
        this.phone !== "" &&
        this.address1 !== "" &&
        this.address2 !== "" &&
        this.city !== "" &&
        this.county !== ""
      );
    }
    return (
      this.first_name !== "" &&
      this.last_name !== "" &&
      this.email !== "" &&
      this.phone !== ""
    );
  }

  renderField(field) {
    switch (field) {
      default: {
        return this[field] || 0;
      }
    }
  }

  renderJSON() {
    return {
      first_name: this.first_name,
      last_name: this.last_name,
      email: this.email,
      phone: this.phone,
      address1: this.address1,
      address2: this.address2,
      city: this.city,
      county: this.county,
    };
  }
}
