import SpecialOffer from "domain-objects/special-offer";

export default class SpecialOfferManager {
  constructor(items = []) {
    this.items = items;
  }

  getSpecialOffer(id) {
    return this.items.find((item) => item.id === id) || new SpecialOffer();
  }

  filter(type = "all") {
    if (type === "all") return this.data;
    else return this.items.filter((specialOffer) => specialOffer.type === type);
  }
}
