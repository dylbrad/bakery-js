export default class Recipe {
  constructor({ id, recipe } = {}) {
    this.id = id;
    this.recipe = recipe;
  }
}
