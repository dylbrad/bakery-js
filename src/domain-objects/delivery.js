import moment from "moment";

import { formatDate, formatDateToBackend } from "utils/date";

export default class Delivery {
  constructor({ delivery_date, time_slot, message = "" } = {}) {
    this.delivery_date = delivery_date;
    this.time_slot = time_slot;
    this.message = message;
  }

  update(field, value) {
    return new Delivery({ ...this, [field]: value });
  }

  isFieldValid(field) {
    switch (field) {
      case "delivery_date": {
        return this[field] !== null;
      }
      default: {
        return this[field] !== "";
      }
    }
  }

  isEmpty() {
    return !this.delivery_date && !this.time_slot;
  }

  hasBooking() {
    return this.delivery_date;
  }

  isValid(isDelivery) {
    return this.delivery_date !== null && !!this.time_slot;
  }

  renderField(field) {
    switch (field) {
      case "delivery_date": {
        return formatDate(this[field]);
      }
      default: {
        return this[field] || 0;
      }
    }
  }

  toMoment(field) {
    return moment(this[field]);
  }

  renderJSON() {
    return {
      delivery_date: formatDateToBackend(this.delivery_date),
      time_slot: this.time_slot,
      message: this.message,
    };
  }
}
