import Delivery from "domain-objects/delivery";
import Trolley from "domain-objects/trolley";
import TrolleyItem from "domain-objects/trolley-item";
import Payment from "domain-objects/payment";
import Customer from "domain-objects/customer";

export default class Order {
  constructor({
    id,
    delivery = new Delivery(),
    trolley = new Trolley(),
    payment = new Payment(),
    customer = new Customer(),
    is_delivery = true,
    inserted_at,
  } = {}) {
    this.id = id;
    this.delivery = delivery;
    this.trolley = trolley;
    this.payment = payment;
    this.customer = customer;
    this.is_delivery = is_delivery;
    this.inserted_at = inserted_at;
  }

  isTrolleyValid(trolley, settingManager) {
    return (
      trolley.numberItems() > 0 &&
      ((this.is_delivery &&
        trolley.calculateTotalWithoutDelivery() >=
          settingManager.getByName("delivery_minimum_order").value) ||
        (!this.is_delivery &&
          trolley.calculateTotalWithoutDelivery() >=
            settingManager.getByName("collection_minimum_order").value))
    );
  }

  renderJSON() {
    return {
      order: {
        id: this.id,
        delivery: this.delivery.renderJSON(),
        trolley: this.trolley,
        payment: this.payment,
        customer: this.customer.renderJSON(),
        is_delivery: this.is_delivery,
      },
    };
  }
}

export function createOrder(params = new Order()) {
  return new Order({
    ...params,
    delivery: new Delivery({
      ...params.delivery,
      delivery_date: new Date(params.delivery.delivery_date),
    }),
    trolley: new Trolley({
      ...params.trolley,
      items: params.trolley.items.map((item) => new TrolleyItem(item)),
    }),
    payment: new Payment(params.payment),
  });
}
