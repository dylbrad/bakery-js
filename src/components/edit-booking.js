import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Grid, Button, Message, Table } from "semantic-ui-react";
import moment from "moment";
import { useHistory } from "react-router-dom";

import Delivery from "domain-objects/delivery";
import {
  fetchBookingSlots,
  clearBookingSlots,
} from "redux/actions/booking-slot-manager";
import {
  fetchEarliestBookingSlot,
  clearBookingSlot,
} from "redux/actions/booking-slot";
import { setDelivery } from "redux/actions/delivery";
import { range } from "utils/util";
import { formatDate, formatDateToBackend, dayOfWeek, amPm } from "utils/date";
import { beginningOfWeek, addDays, removeDays } from "utils/moment";
import IsDeliveryForm from "components/orders/is-delivery-form";

const EditBooking = ({
  fetchBookingSlots,
  bookingSlotManager,
  setDelivery,
  clearBookingSlots,
  clearBookingSlot,
  delivery,
  fetchEarliestBookingSlot,
  bookingSlot,
  order,
  trolley,
  settingManager,
}) => {
  const history = useHistory();

  const [queryDate, setQueryDate] = useState(moment.utc());
  const [stateDelivery, setStateDelivery] = useState(delivery);

  useEffect(() => {
    fetchEarliestBookingSlot();
    return () => clearBookingSlot();
  }, [clearBookingSlot, fetchEarliestBookingSlot]);

  useEffect(() => {
    if (bookingSlot.date) {
      setQueryDate(bookingSlot.toMoment("date"));
      fetchBookingSlots(bookingSlot.date);
    }
    return () => clearBookingSlots();
  }, [bookingSlot, fetchBookingSlots, setQueryDate, clearBookingSlots]);

  const monday = beginningOfWeek(queryDate);

  const previousMonday = removeDays(monday, 7);
  const nextMonday = addDays(monday, 7);

  const today = moment();

  return (
    <Grid>
      <Grid.Row>
        <Grid.Column>
          <IsDeliveryForm />
        </Grid.Column>
      </Grid.Row>
      {!order.isTrolleyValid(trolley, settingManager) && (
        <Grid.Row>
          <Grid.Column>
            {order.is_delivery &&
              !order.isTrolleyValid(trolley, settingManager) && (
                <Message warning>
                  You need to spend €
                  {settingManager.getByName("delivery_minimum_order").value} or
                  more before delivery to get your order delivered
                </Message>
              )}
            {!order.is_delivery &&
              !order.isTrolleyValid(trolley, settingManager) && (
                <Message warning>
                  You need to spend €
                  {settingManager.getByName("collection_minimum_order").value}{" "}
                  or more to place an order for collection.
                </Message>
              )}
          </Grid.Column>
        </Grid.Row>
      )}
      <Grid.Row>
        <Grid.Column>
          <p>
            Please select the date and time at which you want your goods to be
            delivered
          </p>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Button
            onClick={() => {
              setQueryDate(previousMonday);
              fetchBookingSlots(formatDateToBackend(previousMonday));
            }}
            icon="left arrow"
            content="Previous Week"
            labelPosition="left"
            disabled={monday.isBefore(today)}
          />
          <Button
            onClick={() => {
              setQueryDate(nextMonday);
              fetchBookingSlots(formatDateToBackend(nextMonday));
            }}
            icon="right arrow"
            content="Next Week"
            labelPosition="right"
          />
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Table unstackable>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell></Table.HeaderCell>
                <Table.HeaderCell>Monday {formatDate(monday)}</Table.HeaderCell>
                <Table.HeaderCell>
                  Tuesday {formatDate(addDays(monday, 1))}
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Wednesday {formatDate(addDays(monday, 2))}
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Thursday {formatDate(addDays(monday, 3))}
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Friday {formatDate(addDays(monday, 4))}
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Saturday {formatDate(addDays(monday, 5))}
                </Table.HeaderCell>
                <Table.HeaderCell>
                  Sunday {formatDate(addDays(monday, 6))}
                </Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {range(5, 7).map((timeSlot) => {
                const timeSlotEnd = timeSlot + 1;
                return (
                  <Table.Row key={timeSlot}>
                    <Table.Cell
                      style={{ minWidth: "105px", textAlign: "center" }}
                    >
                      {timeSlot}
                      {amPm(timeSlot)} - {timeSlotEnd}
                      {amPm(timeSlotEnd)}
                    </Table.Cell>
                    {range(7).map((day) => {
                      const date = addDays(monday, day);
                      if (
                        bookingSlotManager.getSlots(date, timeSlot).length() > 0
                      ) {
                        return (
                          <Table.Cell
                            key={`${formatDate(date)}-{timeSlot}`}
                            active={
                              stateDelivery
                                .toMoment("delivery_date")
                                .isSame(date, "day") &&
                              stateDelivery.time_slot === timeSlot
                            }
                            onClick={() =>
                              setStateDelivery(
                                new Delivery({
                                  delivery_date: date,
                                  time_slot: timeSlot,
                                })
                              )
                            }
                            style={{
                              cursor: "pointer",
                              fontWeight: 700,
                              fontSize: "1.1em",
                              borderStyle: "solid",
                              borderRadius: "10px",
                            }}
                          >
                            Available
                          </Table.Cell>
                        );
                      } else {
                        return (
                          <Table.Cell
                            style={{
                              fontSize: "0.9em",
                              color: "#969696",
                              cursor: "not-allowed",
                            }}
                            key={`${formatDate(date)}-{timeSlot}`}
                          >
                            No slot available
                          </Table.Cell>
                        );
                      }
                    })}
                  </Table.Row>
                );
              })}
            </Table.Body>
          </Table>
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          {stateDelivery.hasBooking() ? (
            <p>
              You have selected {dayOfWeek(stateDelivery.delivery_date)}{" "}
              {formatDate(stateDelivery.delivery_date)} between{" "}
              {`${stateDelivery.renderField("time_slot")}`.padStart(2, "0")}:00
              {amPm(stateDelivery.time_slot)} and{" "}
              {`${stateDelivery.renderField("time_slot") + 1}`.padStart(2, "0")}
              :00
              {amPm(stateDelivery.time_slot)}.
            </p>
          ) : (
            <p>Pick a date and time to continue</p>
          )}
        </Grid.Column>
      </Grid.Row>
      <Grid.Row>
        <Grid.Column>
          <Button
            disabled={
              !stateDelivery.hasBooking() ||
              !order.isTrolleyValid(trolley, settingManager)
            }
            primary
            onClick={async () => {
              await setDelivery(stateDelivery);
              history.push("/details");
            }}
          >
            Continue to Details
          </Button>
        </Grid.Column>
      </Grid.Row>
    </Grid>
  );
};

const mapStateToProps = ({
  bookingSlotManager,
  delivery,
  bookingSlot,
  order,
  trolley,
  settingManager,
}) => ({
  bookingSlotManager: bookingSlotManager.filterByIsDelivery(order.is_delivery),
  delivery,
  bookingSlot,
  order,
  trolley,
  settingManager,
});

const mapDispatchToProps = {
  fetchBookingSlots,
  setDelivery,
  clearBookingSlots,
  clearBookingSlot,
  fetchEarliestBookingSlot,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditBooking);
