import React from "react";
import { connect } from "react-redux";
import { Button, Card, Grid, Icon, Image } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { addOneProduct } from "redux/actions/trolley";

const ProductCard = ({ product, addOneProduct, trolley, allergenManager }) => {
  const trolleyItem = trolley.findTrolleyItemByProduct(product);
  return (
    <Card style={{ marginLeft: "auto", marginRight: "auto" }}>
      <Image src={product.image} wrapped ui={false} />
      <Card.Content>
        <Card.Header>
          {product.name}
          <span className="right floated">&euro;{product.render("price")}</span>
        </Card.Header>
        <Card.Description>{product.description}</Card.Description>
      </Card.Content>
      {product.allergens.length > 0 && (
        <Card.Content>
          <Card.Header>Contains</Card.Header>
          <Card.Description>
            {allergenManager.items.length > 0 &&
              product.getAllergens(allergenManager).items.map((allergen) => (
                <React.Fragment key={allergen.id}>
                  <Image
                    src={allergen.image}
                    size="mini"
                    style={{ width: "30px" }}
                  />{" "}
                  {allergen.name}{" "}
                </React.Fragment>
              ))}
          </Card.Description>
        </Card.Content>
      )}
      {product.may_contain_allergens.length > 0 && (
        <Card.Content>
          <Card.Header>May contain</Card.Header>
          <Card.Description>
            {allergenManager.items.length > 0 &&
              product
                .getMayContainAllergens(allergenManager)
                .items.map((allergen) => (
                  <React.Fragment key={allergen.id}>
                    <Image
                      src={allergen.image}
                      size="mini"
                      style={{ width: "30px" }}
                    />{" "}
                    {allergen.name}{" "}
                  </React.Fragment>
                ))}
          </Card.Description>
        </Card.Content>
      )}
      <Card.Content>
        <Grid padded>
          <Grid.Row>
            <Grid.Column>
              <Button
                fluid
                basic
                positive
                onClick={() => addOneProduct(product)}
                className="hover"
              >
                <Icon name="shopping cart" />
                Add to trolley {trolleyItem && `(${trolleyItem.quantity})`}
              </Button>
            </Grid.Column>
          </Grid.Row>
          <Grid.Row>
            <Grid.Column>
              <Button
                basic
                color="blue"
                fluid
                as={Link}
                to={`/products/${product.id}/recipe`}
                className="hover"
              >
                <Icon name="list" />
                View Recipe
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Card.Content>
    </Card>
  );
};

const mapStateToProps = ({ trolley, allergenManager }) => ({
  trolley,
  allergenManager,
});

export default connect(mapStateToProps, { addOneProduct })(ProductCard);
