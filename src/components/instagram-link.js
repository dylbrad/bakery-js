import React from "react";
import { connect } from "react-redux";
import { Item } from "semantic-ui-react";

import instagramLogo from "images/instagram.png";

const InstagramLink = ({ instagramURL }) =>
  instagramURL ? (
    <div style={{ overflow: "auto", paddingBottom: "1em" }}>
      <Item.Group style={{ float: "right" }}>
        <Item>
          <Item.Content style={{ paddingLeft: ".5em" }} verticalAlign="middle">
            <Item.Image size="mini" src={instagramLogo} />{" "}
            <Item.Header as="a" href={instagramURL} target="_blank">
              Check us out on Instagram
            </Item.Header>
          </Item.Content>
        </Item>
      </Item.Group>
    </div>
  ) : (
    ""
  );

const mapStateToProps = ({ settingManager }) => {
  return {
    instagramURL: settingManager.getByName("instagram_url").value,
  };
};

export default connect(mapStateToProps)(InstagramLink);
