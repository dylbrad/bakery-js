import React from "react";
import { connect } from "react-redux";
import { Icon } from "semantic-ui-react";

const TrolleyButton = ({ trolley }) => {
  return (
    <>
      <Icon name="cart" />
      <span>
        {trolley.numberItems()} items: &euro;
        {trolley.calculateTotal("string")}
      </span>
    </>
  );
};

const mapStateToProps = ({ trolley }) => ({ trolley });

export default connect(mapStateToProps)(TrolleyButton);
