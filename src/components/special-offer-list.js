import React from "react";
import { connect } from "react-redux";
import { Grid, Segment } from "semantic-ui-react";

import SpecialOfferCard from "components/special-offer-card";

const SpecialOfferList = ({
  trolley,
  message,
  specialOfferManager,
  fetchSpecialOffers,
}) => (
  <Segment basic>
    <Grid stackable columns={3}>
      {specialOfferManager.items.map((specialOffer) => (
        <Grid.Column key={specialOffer.name}>
          <SpecialOfferCard specialOffer={specialOffer} />
        </Grid.Column>
      ))}
    </Grid>
    <div style={{ height: 70 }} />
  </Segment>
);

const mapStateToProps = ({ trolley, message, specialOfferManager }) => ({
  trolley,
  message,
  specialOfferManager,
});

export default connect(mapStateToProps)(SpecialOfferList);
