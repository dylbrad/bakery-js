import React from "react";
import { connect } from "react-redux";
import { Button, Card, Grid, Header, Icon, Image } from "semantic-ui-react";

import { addOneSpecialOffer } from "redux/actions/trolley";

const SpecialOfferCard = ({
  specialOffer,
  addOneSpecialOffer,
  trolley,
  allergenManager,
  productManager,
}) => {
  const trolleyItem = trolley.findTrolleyItemBySpecialOffer(specialOffer);
  return (
    <Card style={{ marginLeft: "auto", marginRight: "auto" }}>
      <Image src={specialOffer.image} wrapped ui={false} />
      <Card.Content>
        <Card.Header>
          {specialOffer.name}
          <span className="right floated">
            &euro;{specialOffer.render("price")}
          </span>
        </Card.Header>
      </Card.Content>
      <Card.Content>
        <Card.Description>
          <Header>Content</Header>
          <ul>
            {productManager.items.length > 0 &&
              specialOffer
                .getProducts(productManager)
                .groupByProduct()
                .map(({ product, quantity }) => (
                  <li key={product.id}>
                    {product.name} <strong>x{quantity}</strong>
                  </li>
                ))}
          </ul>
        </Card.Description>
      </Card.Content>
      {allergenManager.items.length > 0 &&
        specialOffer.getProducts(productManager).getAllergens(allergenManager)
          .items.length > 0 && (
          <Card.Content>
            <Header>Contains</Header>
            {allergenManager.items.length > 0 &&
              specialOffer
                .getProducts(productManager)
                .getAllergens(allergenManager)
                .items.map((allergen) => (
                  <React.Fragment key={allergen.id}>
                    <Image
                      src={allergen.image}
                      size="mini"
                      style={{ width: "30px" }}
                    />{" "}
                    {allergen.name}{" "}
                  </React.Fragment>
                ))}
          </Card.Content>
        )}
      {specialOffer
        .getProducts(productManager)
        .getMayContainAllergens(allergenManager).items.length > 0 && (
        <Card.Content>
          <Header>May contain</Header>
          {allergenManager.items.length > 0 &&
            specialOffer
              .getProducts(productManager)
              .getMayContainAllergens(allergenManager)
              .items.map((allergen) => (
                <React.Fragment key={allergen.id}>
                  <Image
                    src={allergen.image}
                    size="mini"
                    style={{ width: "30px" }}
                  />{" "}
                  {allergen.name}{" "}
                </React.Fragment>
              ))}
        </Card.Content>
      )}
      <Card.Content>
        <Grid padded>
          <Grid.Row>
            <Grid.Column>
              <Button
                fluid
                basic
                positive
                onClick={() => addOneSpecialOffer(specialOffer)}
                className="hover"
              >
                <Icon name="shopping cart" />
                Add to trolley {trolleyItem && `(${trolleyItem.quantity})`}
              </Button>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Card.Content>
    </Card>
  );
};

const mapStateToProps = ({ trolley, allergenManager, productManager }) => ({
  trolley,
  allergenManager,
  productManager,
});

const mapDispatchToProps = {
  addOneSpecialOffer,
};

export default connect(mapStateToProps, mapDispatchToProps)(SpecialOfferCard);
