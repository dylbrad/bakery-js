import React from "react";
import { connect } from "react-redux";
import { Segment, Step, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";

const StepBar = ({ step, trolley, delivery, payment, customer, order }) => (
  <Segment textAlign="center" basic>
    <Step.Group size="mini">
      <Step active={step === "view-trolley"} as={Link} to="/view-trolley">
        <Icon name="shopping cart" />
        <Step.Content>
          <Step.Title>View Trolley</Step.Title>
        </Step.Content>
      </Step>

      <Step
        disabled={trolley.isEmpty()}
        active={step === "booking"}
        as={Link}
        to="/booking/"
      >
        <Icon name="calendar alternate outline" />
        <Step.Content>
          <Step.Title>Booking</Step.Title>
        </Step.Content>
      </Step>

      <Step
        disabled={trolley.isEmpty() || !delivery.hasBooking()}
        active={step === "delivery"}
        as={Link}
        to="/customer/"
      >
        <Icon name="clipboard list" />
        <Step.Content>
          <Step.Title>Details</Step.Title>
        </Step.Content>
      </Step>

      <Step
        disabled={
          trolley.isEmpty() ||
          !delivery.hasBooking() ||
          !customer.isValid(order.is_delivery)
        }
        active={step === "payment"}
        as={Link}
        to="/payment/"
      >
        <Icon name="money" />
        <Step.Content>
          <Step.Title>Payment</Step.Title>
        </Step.Content>
      </Step>

      <Step disabled={step !== "order-placed"} active={step === "order-placed"}>
        <Icon name="info" />
        <Step.Content>
          <Step.Title>Order Placed</Step.Title>
        </Step.Content>
      </Step>
    </Step.Group>
  </Segment>
);

const mapStateToProps = ({ trolley, delivery, payment, customer, order }) => ({
  trolley,
  delivery,
  payment,
  customer,
  order,
});

export default connect(mapStateToProps)(StepBar);
