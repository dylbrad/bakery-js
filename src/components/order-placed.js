import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Header } from "semantic-ui-react";

import StepBar from "components/step-bar";
import { clearOrder } from "redux/actions/order";
import { clearDelivery } from "redux/actions/delivery";
import { clearPayment } from "redux/actions/payment";
import { clearTrolley } from "redux/actions/trolley";

const OrderPlaced = ({
  order,
  clearOrder,
  clearDelivery,
  clearPayment,
  clearTrolley,
}) => {
  useEffect(() => {
    function clear() {
      clearOrder();
      clearDelivery();
      clearPayment();
      clearTrolley();
    }
    return () => clear();
  }, [clearOrder, clearDelivery, clearPayment, clearTrolley]);
  return (
    <div>
      <StepBar step="order-placed" />
      <Header as="h3" icon textAlign="center">
        <Header.Content>Thank you for your order!</Header.Content>
      </Header>
      <p>
        We are sending you an email with a confirmation link to this address:{" "}
        {order.customer.email}.
      </p>
      <p>
        We need you to confirm your email address so that we don't receive too
        many spam orders.
      </p>
      <p>Thank you for your understanding.</p>
    </div>
  );
};

const mapStateToProps = ({ order }) => ({ order });

const mapDispatchToProps = {
  clearOrder,
  clearDelivery,
  clearPayment,
  clearTrolley,
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderPlaced);
