import React from "react";
import { Grid } from "semantic-ui-react";

const Delivery = ({ trolley }) => {
  return (
    trolley.items.length > 0 && (
      <>
        <Grid.Column width={4} />
        <Grid.Column width={8} textAlign="right">
          Delivery
        </Grid.Column>
        <Grid.Column width={4} textAlign="right">
          &euro;{trolley.delivery_price}
        </Grid.Column>
      </>
    )
  );
};

export default Delivery;
