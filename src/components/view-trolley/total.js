import React from "react";
import { Grid } from "semantic-ui-react";

const Total = ({ trolley }) => (
  <>
    <Grid.Column width={2} />
    <Grid.Column width={10} textAlign="right">
      Total
    </Grid.Column>
    <Grid.Column width={4} textAlign="right">
      &euro;{trolley.calculateTotal("string")}
    </Grid.Column>
  </>
);

export default Total;
