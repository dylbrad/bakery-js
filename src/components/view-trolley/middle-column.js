import React from "react";
import { Grid, Header, Button } from "semantic-ui-react";

export default ({
  item,
  trolleyItem,
  addOneItem,
  removeOneItem,
  productManager,
}) => (
  <Grid>
    <Grid.Row>
      <Grid.Column>
        <Header>{item.name}</Header>
        <span>&euro;{trolleyItem.render("unit_price")} / unit</span>
      </Grid.Column>
    </Grid.Row>
    {!trolleyItem.isProduct() && (
      <Grid.Row>
        <Grid.Column style={{ fontSize: "0.85em" }}>
          Content:
          <ul>
            {item
              .getProducts(productManager)
              .groupByProduct()
              .map(({ product, quantity }) => (
                <li key={product.id}>
                  {product.name} x{quantity}
                </li>
              ))}
          </ul>
        </Grid.Column>
      </Grid.Row>
    )}
    <table>
      <tbody>
        <tr>
          <td style={{ width: "100px" }}>Quantity</td>
          <td style={{ width: "40px", textAlign: "center" }}>
            <Button basic fluid size="mini" onClick={() => removeOneItem(item)}>
              -
            </Button>
          </td>
          <td style={{ width: "70px", textAlign: "center" }}>
            {trolleyItem.quantity}
          </td>
          <td style={{ width: "40px" }}>
            <Button basic fluid size="mini" onClick={() => addOneItem(item)}>
              +
            </Button>
          </td>
        </tr>
      </tbody>
    </table>
  </Grid>
);
